
# coding: utf-8

# In[ ]:

"""
Created on Mon Apr  6 09:43:32 2015

@author: reine
This code is for the solution of Henon Heleis  Hamiltonian:
 H=0.5*(px^2+py^2)+0.5*(AA*x^2+BB*y^2)+CC*x^2*y-DD*y^3/3.
 ( The usual Hamiltonian has AA=BB=CC=DD=1 )
Here we solve a system of 8ODES couple together and GALI4 for 4 deviation vectors wk of x.
Each vector is 4 components """

from __future__ import division
from numpy import zeros, linspace, array, log, sum, dot, matrix,diag
from numpy import absolute as abs
from numpy.linalg import svd
from numpy.linalg  import norm
from scipy.integrate import ode
import random
import time
random.seed("time")

"""define a file for writting data as text to an external file "mydata"""
random.seed("time")
myfile1 = open('MydataG4r31', 'w') 
myfile2 = open('MydataG4r32', 'w')

"""Define a gram schmith function to othonormalise input vectors to eah others (with norm 1)"""
def Gram(U):
    V=[]
    W=[]
    V.append(U[0])
    W.append(V[0]/norm(V[0]))
    for i in range(1,len(U)):
        v=array(U[i])
        for j in range(i):
            v=v-(dot(U[i],V[j])/norm(V[j])**2)*array(V[j])
        V.append(list(v))  
        W.append(v/norm(v))    
    return W
    
"""define initial conditions """
H0= 0.10
x0 = 0
y0 = 0.20
#px0=0.5
py0 = 0.05
# defined px as a functions of Hamitonian and others coordinates
px0 = (2*H0- x0**2 -y0**2- 2*(x0**2)*y0+2*(y0**3)/3 -py0**2)**0.5
# Random choice of the  n initial deviation vectors
w01 = [random.uniform(-1e-00,1) for i in range(4)]
w02 = [random.uniform(-1e-00,1) for i in range(4)]
w03 = [random.uniform(-1e-00,1) for i in range(4)]
w04 = [random.uniform(-1e-00,1) for i in range(4)]
U=[w01,w02,w03,w04]
# Orthonormalisation of the initial deviation vector
wa=Gram(U)
w001=wa[0]
w002=wa[1]
w003=wa[2]
w004=wa[3]
# initial position of the orbit
X0 = [x0,y0,px0,py0]
ic = list(X0)+ list(w001)+list(w002)+list(w003)+list(w004)
"""Values of the parameters and Time to simulate system over"""
epsi=10
tau = 1e-2
Tm = 1e+4
SF=0
k=1
OC="Regular"
Gm=1e-16
time=[]
GALIt=[]

""" Define the  8 ODEs satisfied by x and w"""
def hamil_sys(t, q):
    #x,y,px ,py ,dx1,dy1,dpx1, dpy1,dx2,dy2,dpx2, dpy2, dx3,dy3,dpx3, dpy3, dx4,dy4,dpx4, dpy4=q
    xdot=q[2]            # px
    ydot=q[3]            #py
    pxdot=-q[0]-2*q[0]*q[1]    #-x-2*x*y
    pydot=-q[1]-q[0]**2+q[1]**2 #-y-x**2+y**2
    dx1dot=q[6]      #dpx1
    dy1dot=q[7]      #dpy1  
    dpx1dot=(-1-2*q[1])*q[4] + (-2*q[0])*q[5]  #(-1-2*y)*dx1 + (-2*x)*dy1
    dpy1dot=(-2*q[0])*q[4]+(-1+2*q[1])*q[5]    #(-2*x)*dx1+(-1+2*y)*dy1
    dx2dot=q[10]      #dpx2
    dy2dot=q[11]      #dpy2  
    dpx2dot=(-1-2*q[1])*q[8] + (-2*q[0])*q[9]  #(-1-2*y)*dx2 + (-2*x)*dy2
    dpy2dot=(-2*q[0])*q[8]+(-1+2*q[1])*q[9]    #(-2*x)*dx2+(-1+2*y)*dy2
    dx3dot=q[14]      #dpx3
    dy3dot=q[15]      #dpy3  
    dpx3dot=(-1-2*q[1])*q[12] + (-2*q[0])*q[13]  #(-1-2*y)*dx3 + (-2*x)*dy3
    dpy3dot=(-2*q[0])*q[12]+(-1+2*q[1])*q[13]    #(-2*x)*dx3+(-1+2*y)*dy3
    dx4dot=q[18]      #dpx4
    dy4dot=q[19]      #dpy4  
    dpx4dot=(-1-2*q[1])*q[16] + (-2*q[0])*q[17]  #(-1-2*y)*dx4 + (-2*x)*dy4
    dpy4dot=(-2*q[0])*q[16]+(-1+2*q[1])*q[17]    #(-2*x)*dx4+(-1+2*y)*dy4    
    return [xdot,ydot,pxdot,pydot,dx1dot, dy1dot,dpx1dot,dpy1dot,dx2dot, dy2dot,dpx2dot,dpy2dot, dx3dot, dy3dot,dpx3dot,dpy3dot,dx4dot, dy4dot,dpx4dot,dpy4dot] # the solution of the ODEs at each times   

""" Solve the system with the integrator dop853 and step tau"""   
while (SF==0):
    solver = ode(hamil_sys)
    solver.set_initial_value(ic,(k-1)*tau)
    solver.set_integrator('dop853')
    solver.integrate(solver.t + tau/epsi)
   
    sol2=solver.y
    ha=0.50*(sol2[2]**2 +sol2[3]**2+sol2[0]**2+sol2[1]**2+2*(sol2[0]**2)*sol2[1] -2*(sol2[1]**3)/float(3))
    RE= abs((ha-H0)/H0)
    X = [sol2[0],sol2[1],sol2[2],sol2[3]]
    w1 = [sol2[4],sol2[5],sol2[6],sol2[7]]
    w2 = [sol2[8],sol2[9],sol2[10],sol2[11]]
    w3 = [sol2[12],sol2[13],sol2[14],sol2[15]]
    w4 = [sol2[16],sol2[17],sol2[18],sol2[19]]
    # Normalisation of the deviation vectors
    w1 = w1/norm(w1)
    w2 = w2/norm(w2)
    w3 = w3/norm(w3)
    w4 = w4/norm(w4)
    # Define the marix A to contend the deviations vectors
    A = matrix([w1,w2,w3,w4])
    #Compute the singular value decomposition of A
    u,z,v= svd(A.T)
    # Compute and store the GALI
    GALI =z.prod()
    GALIt.append(GALI)
    time.append(k*tau)
    # to  stores data (time,SALI,hamiltonian, relative error,  position and devition vector) at each iteration
    myfile1.write(str(k*tau) + " " + str(GALI) +"  "+  str(log(GALI)) +"  "+ str(ha) + "  " + str(abs((ha-H0)/H0)) +  "\n")    
    myfile2.write(str(sol2[0]) + " " + str(sol2[1])+" "+ str(sol2[2]) +" "+ str(sol2[3]) + " " + str(sol2[4]) + " " + str(sol2[5]) + " " + str(sol2[6]) + " " + str(sol2[7]) +" " + str(sol2[8]) + " " + str(sol2[9]) + " " + str(sol2[10]) + " " + str(sol2[11]) +" " + str(sol2[12]) + " " + str(sol2[13]) + " " + str(sol2[14]) + " " + str(sol2[15])+" " + str(sol2[16]) + " " + str(sol2[17]) + " " + str(sol2[18]) + " " + str(sol2[19]) +"\n")
    # Reinitialisation of the initial conditions with the new w
    ic= list(X)+list(w1)+list(w2)+list(w3)+list(w4)
    # Incrementation of the counter (step)
    k = k+1
    # Define the stopping condition  for the CPU time or the GALI2
    if GALIt[k-2] < Gm:
        SF=1
        OC="Chaotic"
    if k*tau > Tm:
        
        SF=1    
   
myfile1.close() # Remember a line for closing the file    
myfile2.close()
print OC # Output the nature of the orbit


# In[ ]:



