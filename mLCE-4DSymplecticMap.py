
# coding: utf-8

# In[2]:


"""
Created on Mon Apr  6 09:43:32 2015

@author: reine
This code is for the solution of 4D symplectic map
"""

from __future__ import division
from numpy import zeros, linspace, array, log, sum, dot, cos, sin, mod,pi
from numpy import absolute as abs
from numpy.linalg  import norm
from scipy.integrate import ode
import random

"""define a file for writting data as text to an external file "mydata"""
myfile1 = open('Mydata11', 'w') 
myfile2 = open('Mydata12', 'w')
    
"""define initial conditions """
x10 = 0.5
x20 = 0
x30 = 0.5
x40 = 0
#U=[[random.uniform(-1e-00,1) for i in range(4)] for j in range(n)] # Random choice of the  n initial deviation vectors
# initial position of the orbit
X0 = [x10,x20,x30,x40]  
# Random choice of the  initial deviation vector
w0 = [random.uniform(-1e-00,1) for i in range(4)]
# Normalisation of the initial deviation vector
w00 = w0/norm(w0)  
ic= list(X0)+list(w00) 
"""Values of the parameters and Time to simulate system over """
nu=0.5
mu=1e-3
kappa=0.1
epsi=10
tau = 1e-2
Tm = 1e+4
SF=0
k=1
ww=[norm(w0)]
LCE=[]
time=[]
X1m=1e-10

""" Define the  ODEs satisfied by x and w"""
def sympmap(t, q):
    a=-nu*cos(q[0]+q[1])-mu*sin(q[0]+q[1]+q[2]+q[3])
    b= -mu*sin(q[0]+q[1]+q[2]+q[3])
    c=-kappa*cos(q[2]+q[3])-mu*sin(q[0]+q[1]+q[2]+q[3])
    #M=[[1,1,0,0],[a,1+a,b,b],[0,0,1,1],[b,b,c,c+1]]
    x1dot= mod(q[0]+ q[1] , 2*pi)           # (x1+x2)mod(2*pi)
    x2dot= mod(q[1]-nu*sin(q[0]+q[1])-mu*(1-cos(q[0]+q[1]+q[2]+q[3])), 2*pi)           
    x3dot= mod(q[2]+q[3], 2*pi)     #x3+x4
    x4dot= mod(q[3]-kappa*sin(q[2]+q[3])-mu*(1-cos(q[0]+q[1]+q[2]+q[3])), 2*pi)  
    dx11dot=q[4]+q[5]
    dx21dot=a*q[4]+(1+a)*q[5]+b*q[6]+b*q[7]
    dx31dot=q[6]+q[7]
    dx41dot=b*q[4]+b*q[5]+c*q[6]+(1+c)*q[7]
    q=[x1dot,x2dot,x3dot,x4dot, dx11dot,dx21dot, dx31dot, dx41dot]
    
    # the solution of the ODEs at each times
    return q

""" Solve the system with the integrator dop853 and step tau"""   
while (SF==0):
    sol= sympmap(k*tau, ic)  # Store the result of the integration in sol
    # to compute the position x and the deviation vectors w 
    X = [sol[0],sol[1],sol[2],sol[3]]
    w = [sol[4],sol[5],sol[6],sol[7]]
    alpha=norm(w)
    ww.append(alpha) 
    # to compute the lyapunov characteristic exponent and store in LCE 
    X1=sum(log(ww))/(k*tau)
    LCE.append(X1) 
    time.append(k*tau)
    # to  stores data (time, mLCE,hamiltonian, relative error,  position and devition vector) at each iteration
    myfile1.write(str(k*tau) + " " + str(X1) +"  "+  str(log(k*tau)) + " " + str(log(X1))+  "\n")
    myfile2.write(str(sol[0]) + " " + str(sol[1])+" "+ str(sol[2]) +" "+ str(sol[3]) + " " + str(sol[4]) + " " + str(sol[5]) + " " + str(sol[6]) + " " + str(sol[7]) +"\n")
    # Normalisation of the deviation vectors
    w = w/alpha 
    # Reinitialisation of the initial conditions with the new w
    ic= list(X)+list(w) 
    # Incrementation of the counter (step)
    k = k+1 
    # Define the stopping condition  for the CPU time or the mLCE
    if k*tau > Tm or LCE[k-2]< X1m :   
        SF=1   
myfile1.close() # A line for closing the storage file  
myfile2.close()


# In[ ]:

import pylab as pb
from numpy import log
import numpy as np
ar1=np.loadtxt("Mydata11")
ar2=np.loadtxt("Mydata21")
ar3=np.loadtxt("Mydata31")
ar4=np.loadtxt("Mydata41")
    
l1,l2,l3,l4= pb.loglog( ar1[:,0], ar1[:,1], ar2[:,0],ar2[:,1],ar3[:,0],ar3[:,1], ar4[:,0], ar4[:,1])
pb.legend((l1, l2,l3,l4), ( r"1. $x_0=0.5,\,y_0=0,\,p_{x_0}=0.5,\,p_{y_0}=0$",r"2. $x_0=3,\,  y_0=0,\,p_{x_0}=0.5,\, p_{y_0}=0$", r"3. $x_0=3.5,\,y_0=0,\,p_{x_0}=0.5,\,p_{y_0}=0$",r"4. $x_0=4,\,  y_0=0,\,p_{x_0}=0.5,\, p_{y_0}=0$"), 'lower right')
pb.text(1e+3,3e-5,r"Initial Conditions ",horizontalalignment='center', fontsize=15)
pb.text(1e+3,5e-6,r"$H_0=0.125$",horizontalalignment='center', fontsize=15)
pb.text(800,1.2,r"1",horizontalalignment='center', fontsize=14)
pb.text(4,0.0002,r"2",horizontalalignment='center', fontsize=14)
pb.text(16.7,0.0007,r"3 ",horizontalalignment='center', fontsize=14)
pb.text(800,0.004,r"4",horizontalalignment='center', fontsize=14)
pb.ylabel(r"mLCE $\mathbf{X}_1$")
pb.xlabel(r"Time $t$")
#pb.xlim([5e-1,1.4e+4])
#pb.ylim([1e-9,1e+1])
pb.show()

