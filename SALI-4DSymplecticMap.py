
# coding: utf-8

# In[16]:

"""
Created on Mon May  11 09:43:32 2015

@author: reine
This code is for the solution of 4D symplectic map
"""

from __future__ import division
from numpy import zeros, linspace, array, log, sum, dot, cos, sin, mod,pi
from numpy import absolute as abs
from numpy.linalg  import norm
from scipy.integrate import ode
import random

"""define a file for writting data as text to an external file "mydata"""
random.seed("time")
myfile1 = open('Mydatasm41', 'w') 
myfile2 = open('Mydatasm42', 'w')
"""Define a gram schmith function to othonormalise input vectors to eah others (with norm 1)"""
def Gram(U):
    V=[]
    W=[]
    V.append(U[0])
    W.append(list(V[0]/norm(V[0])))
    for i in range(1,len(U)):
        v=array(U[i])
        for j in range(i):
            v=v-(dot(U[i],V[j])/norm(V[j])**2)*array(V[j])
        V.append(list(v))  
        W.append(list(v/norm(v)))    
    return W
    
"""define initial conditions """
n=2 # Number of deviation vectors
x10 = 3.5
x20 = 0
x30 = 0.5
x40 = 0
#U=[[random.uniform(-1e-00,1) for i in range(4)] for j in range(n)] # Random choice of the  n initial deviation vectors
U=[[1,1,1,1],[1,0,0,0]]
X0 = [x10,x20,x30,x40]  # initial position of the orbit
ic=X0+list(array(Gram(U)).reshape(4*n)) 
"""Values of the parameters and Time to simulate system over """
nu=0.5
mu=1e-3
kappa=0.1
epsi=10
tau = 1e-2
Tm = 1e+4
SF=0
k=1
OC="Regular"
Sm=1e-8
time=[]
SALIt=[]

""" Define the  ODEs satisfied by x and w"""
def sympmap(t, q):
    a=-nu*cos(q[0]+q[1])-mu*sin(q[0]+q[1]+q[2]+q[3])
    b= -mu*sin(q[0]+q[1]+q[2]+q[3])
    c=-kappa*cos(q[2]+q[3])-mu*sin(q[0]+q[1]+q[2]+q[3])
    #M=[[1,1,0,0],[a,1+a,b,b],[0,0,1,1],[b,b,c,c+1]]
    x1dot= mod(q[0]+ q[1] , 2*pi)           # (x1+x2)mod(2*pi)
    x2dot= mod(q[1]-nu*sin(q[0]+q[1])-mu*(1-cos(q[0]+q[1]+q[2]+q[3])), 2*pi)           
    x3dot= mod(q[2]+q[3], 2*pi)     #x3+x4
    x4dot= mod(q[3]-kappa*sin(q[2]+q[3])-mu*(1-cos(q[0]+q[1]+q[2]+q[3])), 2*pi)  
    dx11dot=q[4]+q[5]
    dx21dot=a*q[4]+(1+a)*q[5]+b*q[6]+b*q[7]
    dx31dot=q[6]+q[7]
    dx41dot=b*q[4]+b*q[5]+c*q[6]+(1+c)*q[7]
    dx12dot=q[8]+q[9]
    dx22dot=a*q[8]+(1+a)*q[9]+b*q[10]+b*q[11]
    dx32dot=q[10]+q[11]
    dx42dot=b*q[8]+b*q[9]+c*q[10]+(1+c)*q[11]
    q=[x1dot,x2dot,x3dot,x4dot, dx11dot,dx21dot, dx31dot,dx41dot,dx12dot,dx22dot, dx32dot,dx42dot]
    
    # the solution of the ODEs at each times
    return q

""" Solve the system with the integrator dop853 and step tau"""   
while (SF==0):   
    sol= sympmap(k*tau, ic) # Store the result of the integration in sol
    # to compute the position x and the deviation vectors w 
    X = sol[0:4]
    w1 = sol[4:8]
    w2 = sol[8:12]
    
    # Normalisation of the deviation vectors
    w1 = w1/norm(w1)
    w2 = w2/norm(w2)
    # to compute the SALI and store
    SALI = min(norm(w1+w2),norm(w1-w2))
    SALIt.append(SALI)
    time.append(k*tau)
        
    # to  stores data (time,SALI,hamiltonian, relative error,  position and deviation vectors) at each iteration
    myfile1.write(str(k*tau) + " " + str(SALI) + " " + str(log(k*tau)) + "  " +  str(log(SALI)) +"\n")    
    myfile2.write(str(sol[0]) + " " + str(sol[1])+" "+ str(sol[2]) +" "+ str(sol[3]) + " " + str(sol[4]) + " " + str(sol[5]) + " " + str(sol[6]) + " " + str(sol[7]) +" " + str(sol[8]) + " " + str(sol[9]) + " " + str(sol[10]) + " " + str(sol[11]) +"\n")
    
    # Reinitialisation of the initial conditions with the new normalised w fo the next calculation
    ic = list(X)+list(w1)+list(w2)
    # Incrementation of the counter (step)
    k = k+1
    # Define the stopping condition  for  the SALI  or the CPU time 
    if SALIt[k-2] < Sm :
        SF=1
        OC="Chaotic"
    if k*tau > Tm:    
        SF=1    
   
myfile1.close() # A line for closing the storage file  
myfile2.close()
print OC  # output the nature of the orbit

    
    
    
    
    
    


# In[26]:

import pylab as pb
from numpy import log
import numpy as np
ar1=np.loadtxt("Mydatasm11")
ar2=np.loadtxt("Mydatasm21")
ar3=np.loadtxt("Mydatasm31")
ar4=np.loadtxt("Mydatasm41")
    
l1,l2,l3,l4= pb.loglog( ar1[:,0], ar1[:,1], ar3[:,0],ar3[:,1],ar4[:,0],ar4[:,1], ar2[:,0], ar2[:,1])
pb.legend((l1, l2,l3,l4), ( r"1. $x_0=0.5,\,y_0=0,\,p_{x_0}=0.5,\,p_{y_0}=0$",r"2. $x_0=3,\,  y_0=0,\,p_{x_0}=0.5,\, p_{y_0}=0$", r"3. $x_0=3.5,\,y_0=0,\,p_{x_0}=0.5,\,p_{y_0}=0$",r"4. $x_0=4,\,  y_0=0,\,p_{x_0}=0.5,\, p_{y_0}=0$"), 'lower right')
pb.text(1e+3,3e-5,r"Initial Conditions ",horizontalalignment='center', fontsize=15)
pb.text(1e+3,5e-6,r"$H_0=0.125$",horizontalalignment='center', fontsize=15)
pb.text(800,1.2,r"1",horizontalalignment='center', fontsize=14)
pb.text(4,0.0002,r"2",horizontalalignment='center', fontsize=14)
pb.text(16.7,0.0007,r"3 ",horizontalalignment='center', fontsize=14)
pb.text(800,0.004,r"4",horizontalalignment='center', fontsize=14)
#pb.title("SALI")
pb.ylabel(r"SALI")
pb.xlabel(r"Time $t$")
pb.xlim([5e-1,1.4e+4])
pb.ylim([1e-9,1e+1])
pb.show()

