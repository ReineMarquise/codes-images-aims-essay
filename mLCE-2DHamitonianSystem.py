
# coding: utf-8

# In[42]:


"""
Created on Mon Apr  6 09:43:32 2015

@author: reine
This code is for the solution of Henon Heleis  Hamiltonian:
 H=0.5*(px^2+py^2)+0.5*(AA*x^2+BB*y^2)+CC*x^2*y-DD*y^3/3.
 ( The usual Hamiltonian has AA=BB=CC=DD=1 )
Here we solve a system of 8 ODES couple together and compute the maximun Lyapynov exponant (mLCE) for the deviation vector w of x.
Each vector is 4 components
"""

from __future__ import division
from numpy import zeros, linspace, array, log, sum, dot
from numpy import absolute as abs
from numpy.linalg  import norm
from scipy.integrate import ode
import random


"""define a file for writting data as text to an external file "mydata"""
random.seed("time")
myfile1 = open('Mydatach1', 'w') 
myfile2 = open('Mydatach2', 'w')
"""define initial conditions """
H0= 0.05
x0 = 0.0
y0 = 0.20
#px0=0.42081
py0 = 0.05
px0 = (2*H0- x0**2 -y0**2- 2*(x0**2)*y0+2*(y0**3)/3 -py0**2)**0.5 # defined px as a functions of Hamitonian and others coordinates
w0 = [random.uniform(-1e-00,1) for i in range(4)] # Random choice of the  initial deviation vectors
w00 = w0/norm(w0)  # Normalisation of the initial deviation vector
X0 = [x0,y0,px0,py0] # initial position of the orbit
ic = list(X0)+ list(w00)  # Total initial conditions for the all set of ODEs (systems)
"""Values of the parameters and Time to simulate system over"""
epsi=10
tau = 0.6
Tm = 1e+5
SF=0
k=1
ww=[norm(w0)]
LCE=[]
X1m=1e-10

""" Define the ODEs satisfied by x and w"""
def symap(t, q):
    #x,y,px ,py ,dx,dy,dpx, dpy=q
    xdot=q[2]            # px
    ydot=q[3]            #py
    pxdot=-q[0]-2*q[0]*q[1]    #-x-2*x*y
    pydot=-q[1]-q[0]**2+q[1]**2 #-y-x**2+y**2
    dxdot=q[6]      #dpx
    dydot=q[7]      #dpy  
    dpxdot=(-1-2*q[1])*q[4] + (-2*q[0])*q[5]  #(-1-2*y)*dx + (-2*x)*dy
    dpydot=(-2*q[0])*q[4]+(-1+2*q[1])*q[5]    #(-2*x)*dx+(-1+2*y)*dy 
    return [xdot,ydot,pxdot,pydot,dxdot, dydot,dpxdot,dpydot] # the solution of the ODEs at each times     

""" Solve the system with the integrator dop853 and step tau""" 
while (SF==0):
    solver = ode(hamil_sys)
    solver.set_initial_value(ic,(k-1)*tau)
    solver.set_integrator('dop853')
    solver.integrate(solver.t + tau/epsi)
    sol=solver.y  # Store the result of the integration in sol
    # to compute the hamiltonian, relative errors, the position x, the deviation vector w and its norm(alpha), store in ww
    ha=0.50*(sol[2]**2 +sol[3]**2+sol[0]**2+sol[1]**2+2*(sol[0]**2)*sol[1] -2*(sol[1]**3)/float(3))
    RE= abs((ha-H0)/H0)
    X = [sol[0],sol[1],sol[2],sol[3]]
    w = [sol[4],sol[5],sol[6],sol[7]]
    alpha=norm(w)
    ww.append(alpha) 
    # to compute the lyapunov characteristic exponent and store in LCE 
    X1=sum(log(ww))/(k*tau)
    LCE.append(X1)   
    # to  stores data (time, mLCE,hamiltonian, relative error,  position and devition vector) at each iteration
    myfile1.write(str(k*tau) + " " + str(X1) +"  "+  str(ha) + "  " + str(abs((ha-H0)/H0)) +  "\n")
    myfile2.write(str(sol[0]) + " " + str(sol[1])+" "+ str(sol[2]) +" "+ str(sol[3]) + " " + str(sol[4]) + " " + str(sol[5]) + " " + str(sol[6]) + " " + str(sol[7]) +"\n")
    w = w/alpha # Normalisation of the deviation vectors
    ic= list(X)+list(w) # Reinitialisation of the initial conditions with the new w
    k = k+1 # Incrementation of the counter (step)
    if k*tau > Tm  or LCE[k-2]< X1m : # Define the stopping condition  for the CPU time or the mLCE  
        SF=1   
myfile1.close() # A line for closing the storage file  
myfile2.close()
    
    
    
    
    
    

